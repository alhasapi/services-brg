from flask import Flask
from flask import jsonify
from flask import request

app = Flask(__name__)

# Fake DB for the sake of simulation
CLIENTS=[
     { 'id':'1', 'name':'Moussa', 'balance': 0 },
     { 'id':'2', 'name':'Solo', 'balance': 14 },
 ]

@app.route('/clientdb/client', methods=['GET'])
def getAllclient():
    return jsonify({'clients':CLIENTS})

@app.route('/clientdb/client/<clientId>', methods=['GET'])
def getclient(clientId):
    usr = [ client for client in CLIENTS if (client['id'] == clientId) ]
    return jsonify({'client':usr})

@app.route('/clientdb/client/<clientId>', methods=['PUT'])
def updateclient(clientId):
    em = [ client for client in CLIENTS if (client['id'] == clientId) ]
    if 'name' in request.json:
        em[0]['name'] = request.json['name']
    if 'balance' in request.json:
        em[0]['balance'] = str(int(em[0]['balance']) + int(request.json['balance']))
    return jsonify({'client':em[0]})

@app.route('/clientdb/client/', methods=['POST'])
def createclient():
    dat = {
        'id':request.json['id'],
        'name':request.json['name'],
        'balance':int(request.json['balance'])
    }
    CLIENTS.append(dat)
    return jsonify(dat)

"""
@app.route('/clientdb/client/<clientId>',methods=['DELETE'])
def deleteclient(clientId):
    em = [ client for client in CLIENTS if (client['id'] == clientId) ]
    if len(em) == 0:
        abort(404)
    CLIENTS.remove(em[0])
    return jsonify({'response':'Success'})
"""

if __name__ == '__main__':
     app.run()
