





# You must first install flask 

_pip install flask_

# Then open a bunch of terminals (3 actually)

From the first terminal you launch service1.py with
_python service1.py_
And from the second terminal you launch service2.py with
_python service2.py_
and finaly from remaining terminal experiment with curl if you want

# Example:

## Direct access to service1

### Checking for a client with 1 as id

_curl -i http://127.0.0.1:3000/clientdb/client/1_

### Creating a client with with the name "Wittgenstein" 
_curl -i -H "Content-type: application/json" -X POST -d "{\"id\":\"33\", \"name\":\"Wittgenstein\", \"balance\":\"500\"}" http://127.0.0.1:5000/clientdb/client/_

### Updating the the balance of a client named "Solo"

_curl -i -H "Content-type: application/json" -X PUT -d "{\"name\":\"Solo\", \"balance\":\"500\"}" http://127.0.0.1:5000/clientdb/client/2_

## Access to service1 through service2

### Checking for a client with 1 as id

_curl -i http://127.0.0.1:3000/clientdb/client/1_

### Crediting the account of a client named "Solo" 500 (whatever currency)

_curl -i -H "Content-type: application/json" -X PUT -d "{\"name\":\"Solo\", \"balance\":\"500\"}" http://127.0.0.1:3000/clientdb/credit/2_

# Chaos!
