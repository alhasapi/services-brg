import requests
from flask import Flask
from flask import jsonify
from flask import request


app = Flask(__name__)


@app.route('/clientdb/client/<clientId>', methods=['GET'])
def doesHeExists(clientId):
    CLIENTS = data = requests.get(f"http://127.0.0.1:5000/clientdb/client/{clientId}").json()
    return jsonify({'client':CLIENTS})


@app.route('/clientdb/credit/<clientId>', methods=['PUT'])
def credit(clientId):
    responce = requests.put(
           f"http://127.0.0.1:5000/clientdb/client/{clientId}", 
           json=request.json
    ).json()
    return jsonify({'client':responce})


if __name__ == '__main__':
     app.run(port=3000)
